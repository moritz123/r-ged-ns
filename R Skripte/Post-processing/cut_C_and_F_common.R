cut_C_and_F_common <- function(dataFrame) 
{
  vec <- NULL
  name <- ""
  pattern <- "^[A-Za-z\\*~]+[^(I|A|C|G|T|a|c|g|t)][A-Za-z\\*~]+$"
  testlen <- 10
  
  if(rownames(dataFrame)[1] == "1" &&
       length(grep(pattern, rownames(dataFrame)[1:testlen])) != testlen) {
     found <- F
     #determine column with regular expression
     for(n in names(dataFrame)) {
       if(length(grep(pattern, dataFrame[,n][1:testlen])) == testlen) {
         vec <- dataFrame[,n]
         name <- n
         found <- T
         break
       }
     }
     if(found == F)
       stop("No pattern match for AA-Seq-column found! pattern: ", pattern)
  }
  else {
    vec <- rownames(dataFrame)
    name <- "rownames"
  }
  
  #pruefe ob ein C am Anfang steht und ein F am Ende
  if(length(grep("^C.+F$", vec[1:testlen])) == testlen)
  {
    if(name=="rownames")  {
      rownames(dataFrame)<-sapply(strsplit("",x=rownames(dataFrame)),
                           function(X)paste(X[-c(1,length(X))],collapse="",sep=""))
    }
    else {
      dataFrame[,name]<-sapply(strsplit("",x=dataFrame[,name]),
                        function(X)paste(X[-c(1,length(X))],collapse="",sep=""))
    }
  }
  #return dataFrame
  return(dataFrame)
}