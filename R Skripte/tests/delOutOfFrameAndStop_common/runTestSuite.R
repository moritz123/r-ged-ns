library('RUnit')

setwd("~/Scripts")

source('delOutOfFrameAndStop_common.R')

test.suite <- defineTestSuite("example",
                              dirs = file.path("tests/delOutOfFrameAndStop_common"),
                              testFileRegexp = '^\\d+\\.R')

test.result <- runTestSuite(test.suite)

printTextProtocol(test.result)
